﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace ModernUI
{
    public class WindowShadow
    {
        public IntPtr WindowHandle { get; private set; }

        public WindowShadow(Window window)
        {
            this.WindowHandle = GetWindowHandle(window);
        }

        public WindowShadow(IntPtr windowHandle)
        {
            this.WindowHandle = windowHandle;
        }

        private IntPtr GetWindowHandle(Window window)
        {
            WindowInteropHelper interopHelper = new WindowInteropHelper(window);
            return interopHelper.Handle;
        }

        private bool isEnabled;
        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set
            {
                this.isEnabled = value;
                if (this.isEnabled) Enable();
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Margins
        {
            public int LeftWidth;
            public int RightWidth;
            public int TopHeight;
            public int BottomHeight;
        }

        /// <summary>
        /// Drops a standard shadow to a WPF Window, even if the window isborderless. Only works with DWM (Vista and Seven).
        /// This method is much more efficient than setting AllowsTransparency to true and using the DropShadow effect,
        /// as AllowsTransparency involves a huge permormance issue (hardware acceleration is turned off for all the window).
        /// </summary>
        /// <param name="window">Window to which the shadow will be applied</param>
        /// <returns>True if the method succeeded, false if not</returns>
        private bool Enable()
        {
            try
            {
                int NcRenderingPolicy = 2;
                int result = DwmNativeMethods.DwmSetWindowAttribute(this.WindowHandle, DwmWindowAttribute.NcRenderingPolicy, ref NcRenderingPolicy, sizeof(int));

                if (result == 0)
                {
                    // Use -1 margins to extend glass to all window
                    Margins margins = new Margins { BottomHeight = -1, LeftWidth = -1, RightWidth = -1, TopHeight = -1 };
                    //Margins margins = new Margins { BottomHeight = 0, LeftWidth = 0, RightWidth = 0, TopHeight = 0 };
                    result = DwmNativeMethods.DwmExtendFrameIntoClientArea(this.WindowHandle, ref margins);
                    return result == 0;
                }
                return false;
            }
            catch (Exception)
            {
                // Probably dwmapi.dll not found (incompatible OS)
                return false;
            }
        }
    }
}
