﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ModernUI
{
    /// <summary>
    /// Interaction logic for AeroGlassWindow.xaml
    /// </summary>
    public partial class AeroSnapWindow : Window
    {
        public AeroSnapWindow()
        {
            InitializeComponent();
            this.FadeOutAnimation = FindResource("FadeOutAnimation") as Storyboard;
            this.ZoomInAnimation = FindResource("ZoomInAnimation") as Storyboard;
        }

        private Storyboard FadeOutAnimation;
        private Storyboard ZoomInAnimation;
        
        public new void Hide()
        {
            if (this.Visibility != System.Windows.Visibility.Hidden)
            {
                this.FadeOutAnimation.Begin(this);
                this.State = WindowSnapState.None;
            }
        }


        // Predefined snap window sizes and locations
        // TODO: Make it work with multiple monitors
        private static readonly Rect AeroSnapWindowFullBounds = new Rect
        {
            Width = SystemParameters.WorkArea.Width - 16,
            Height = SystemParameters.WorkArea.Height - 16,
            X = 8,
            Y = 8
        };

        private static readonly Rect AeroSnapWindowLeftBounds = new Rect
        {
            Width = SystemParameters.WorkArea.Width / 2 - 8,
            Height = SystemParameters.WorkArea.Height - 16,
            X = 8,
            Y = 8
        };

        private static readonly Rect AeroSnapWindowRightBounds = new Rect
        {
            Width = SystemParameters.WorkArea.Width / 2 - 8,
            Height = SystemParameters.WorkArea.Height - 16,
            X = SystemParameters.WorkArea.Width / 2,
            Y = 8
        };

        public Rect InitialBounds
        {
            get { return (Rect)GetValue(InitialBoundsProperty); }
            set { SetValue(InitialBoundsProperty, value); }
        }

        public static readonly DependencyProperty InitialBoundsProperty =
            DependencyProperty.Register("InitialBounds", typeof(Rect), typeof(Window), new PropertyMetadata(new Rect()));

        public Rect FinalBounds
        {
            get { return (Rect)GetValue(FinalBoundsProperty); }
            set { SetValue(FinalBoundsProperty, value); }
        }

        public static readonly DependencyProperty FinalBoundsProperty =
            DependencyProperty.Register("FinalBounds", typeof(Rect), typeof(Window), new PropertyMetadata(new Rect()));

        public WindowSnapState State
        {
            get { return (WindowSnapState)GetValue(StateProperty); }
            set { SetValue(StateProperty, value); }
        }

        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register("State", typeof(WindowSnapState), typeof(Window), new PropertyMetadata(WindowSnapState.None));

        public new void Show(WindowSnapState snapState, Rect currentBounds)
        {
            if (this.Visibility == System.Windows.Visibility.Visible) return;
            
            base.Show();
            
            if (snapState == WindowSnapState.Full)
                this.FinalBounds = AeroSnapWindowFullBounds;
            else if (snapState == WindowSnapState.Left)
                this.FinalBounds = AeroSnapWindowLeftBounds;
            else if (snapState == WindowSnapState.Right)
                this.FinalBounds = AeroSnapWindowRightBounds;

            
            this.InitialBounds = currentBounds;

            this.BeginAnimation(Window.VisibilityProperty, null);
            this.Visibility = System.Windows.Visibility.Visible;
            this.ZoomInAnimation.Begin(this);
            this.State = snapState;
        }
    }
}
