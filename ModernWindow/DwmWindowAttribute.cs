﻿using System;

namespace ModernUI
{
    internal enum DwmWindowAttribute {
        NcRenderingEnabled = 1,
        NcRenderingPolicy,
        TransitionsForcedisabled,
        AllowNcPaint,
        CaptionButtonBounds,
        NonClientRtlLayout,
        ForceIconicRepresentation,
        Flip3dPolicy,
        ExtendedFrameBounds,
        HasIconicBitmap,
        DisallowPeek,
        ExcludedFromPeek,
        Cloak,
        Cloaked,
        FreezeRepresentation,
        Last
    }
}