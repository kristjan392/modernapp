﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace ModernUI
{
    internal class Resize
    {
        private const int WM_SYSCOMMAND = 0x0112;
        private const int SC_SIZE = 0xF000;

        public enum ResizeDirection
        {
            Left = 1,
            Right = 2,
            Top = 3,
            TopLeft = 4,
            TopRight = 5,
            Bottom = 6,
            BottomLeft = 7,
            BottomRight = 8,
        }

        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        public static void ResizeWindow(IntPtr hwnd, ResizeDirection direction)
        {
            SendMessage(hwnd, WM_SYSCOMMAND, (IntPtr)(SC_SIZE + direction), IntPtr.Zero);
        }

        public struct Point
        {
            public int x;
            public int y;
        }

        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(out Point lpPoint);

        public static WindowSnapState GetCursorSnapState()
        {
            Point p = new Point();
            GetCursorPos(out p);
            if (p.x < 3)
            {
                return WindowSnapState.Left;
            }
            else if (p.x > SystemParameters.PrimaryScreenWidth - 3)
            {
                return WindowSnapState.Right;
            }
            else if (p.y < 3)
            {
                return WindowSnapState.Full;
            }
            return WindowSnapState.None;
        }
    }
}
