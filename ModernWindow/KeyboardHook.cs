﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Input;

namespace ModernUI
{
    /// <summary>
    /// Wrapper for WinAPI calls to set keyboard hook. 
    /// Accessing low-level calls allows override default windows behavior, for example Win + D or Alt + F4.
    /// </summary>
    public class KeyboardHook : IDisposable
    {
        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll")]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll")]
        private static extern IntPtr CallNextHookEx(IntPtr hkk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("Kernel32.dll")]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        [StructLayout(LayoutKind.Sequential)]
        internal struct KeyboardEvent
        {
            public int Key;
            public int ScanCode;
            public int Flags;
            public int Time;
            public IntPtr ExtraInfo;
        }

        /// <summary>
        /// Low-level keyboard input events hook id
        /// </summary>
        private const int WH_KEYBOARD_LL = 13;
        /// <summary>
        /// WinAPI message code for keydown event
        /// </summary>
        private const int WM_KEYDOWN = 0x0100;
        private const int WM_SYSKEYDOWN = 0x0104;
        /// <summary>
        /// WinAPI message code for keyup event
        /// </summary>
        private const int WM_KEYUP = 0x0101;
        private const int WM_SYSKEYUP = 0x0105;

        private IntPtr keyboardHook;

        private LowLevelKeyboardProc keyboardProc;

        public KeyboardHook()
        {
            // Set all keystates to false (that is keyup)
            int[] keyCodes = (int[])Enum.GetValues(typeof(Key));
            foreach (int keyCode in keyCodes) this.KeyStates[keyCode] = false;

            ProcessModule currentModule = Process.GetCurrentProcess().MainModule;

            this.keyboardProc = new LowLevelKeyboardProc(CaptureKeyPress);

            this.keyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL,
                    keyboardProc, GetModuleHandle(currentModule.ModuleName), 0);
        }

        /// <summary>
        /// Adds an action corresponding to specified combination of keys to listen for.
        /// </summary>
        /// <param name="action">Action to be invoken when key combination is detected.</param>
        /// <param name="setMessageHandled">If true then message transmission to other hooks and/or target process is disabled.</param>
        /// <param name="keys">Key combination to listen for</param>
        public void SubscribeKeyEvent(Action action, bool setMessageHandled, params Key[] keys)
        {
            this.KeyActionCollection.Add(new KeyAction { Keys = keys, Action = action, SetMessageHandled = setMessageHandled });
        }

        /// <summary>
        /// Adds an action corresponding to specified combination of keys to listen for.
        /// </summary>
        /// <param name="action">Action to be invoken when key combination is detected.</param>
        /// <param name="keys">Key combination to listen for</param>
        public void SubscribeKeyEvent(Action action, params Key[] keys)
        {
            this.SubscribeKeyEvent(action, false, keys);
        }

        /// <summary>
        /// Adds an action corresponding to specified combinations of keys to listen for.
        /// </summary>
        /// <param name="action">Action to be invoken when key combination is detected.</param>
        /// /// <param name="setMessageHandled">If true then message transmission to other hooks and/or target process is disabled.</param>
        /// <param name="keys">Set of key combination to listen for</param>
        public void SubscribeKeyEvent(Action action, bool setMessageHandled, params Key[][] keyCombinations)
        {
            foreach (Key[] keys in keyCombinations)
            {
                this.KeyActionCollection.Add(new KeyAction { Keys = keys, Action = action, SetMessageHandled = setMessageHandled });
            }
        }

        /// <summary>
        /// Adds an action corresponding to specified combinations of keys to listen for.
        /// </summary>
        /// <param name="action">Action to be invoken when key combination is detected.</param>
        /// <param name="keys">Set of key combination to listen for</param>
        public void SubscribeKeyEvent(Action action, params Key[][] keyCombinations)
        {
            this.SubscribeKeyEvent(action, false, keyCombinations);
        }

        internal class KeyAction
        {
            public Key[] Keys { get; set; }
            public bool SetMessageHandled { get; set; }
            public Action Action { get; set; }
        }

        /// <summary>
        /// List of value sets that contain array of keyboard keys to listen for,
        /// indicator whether to prevent message to be passed on to other hooks and 
        /// an action that is to be invoked if match is found.
        /// </summary>
        private List<KeyAction> KeyActionCollection = new List<KeyAction>();

        /// <summary>
        /// Each key in System.Windows.Input.Key enum is mapped to a bool value indicating whether it's up or down.
        /// </summary>
        private bool[] KeyStates = new bool[Enum.GetNames(typeof(Key)).Length];

        /// <summary>
        /// Receives low-level keyboard input events.
        /// </summary>
        /// <param name="nCode">Code used to determine how to process the message. Handle only values >= 0.</param>
        /// <param name="wParam">Keyboard message identifier (WinAPI message code)</param>
        /// <param name="lParam">Pointer to KeyboardEvent struct</param>
        /// <returns></returns>
        private IntPtr CaptureKeyPress(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0)
            {
                KeyboardEvent keyboardEvent = (KeyboardEvent)Marshal.PtrToStructure(lParam, typeof(KeyboardEvent));
                Key pressedKey = KeyInterop.KeyFromVirtualKey(keyboardEvent.Key);
                bool setMessageHandled = false;
                
                if ((int)wParam == WM_KEYDOWN || (int)wParam == WM_SYSKEYDOWN)
                {
                    this.KeyStates[(int)pressedKey] = true;

                    foreach (KeyAction keyAction in this.KeyActionCollection)
                    {
                        bool isKeyMatch = true;
                        foreach (Key key in keyAction.Keys)
                        {
                            if (!this.KeyStates[(int)key])
                            {
                                isKeyMatch = false;
                                break;
                            }
                        }
                        if (isKeyMatch)
                        {
                            keyAction.Action.Invoke();
                            setMessageHandled = keyAction.SetMessageHandled;
                        }
                    }
                }
                else if ((int)wParam == WM_KEYUP || (int)wParam == WM_SYSKEYUP)
                {
                    this.KeyStates[(int)pressedKey] = false;
                }

                if(setMessageHandled) return (IntPtr)1;
            }
            return CallNextHookEx(this.keyboardHook, nCode, wParam, lParam);
        }

        public void Dispose()
        {
            if (this.keyboardHook != IntPtr.Zero)
            {
                UnhookWindowsHookEx(this.keyboardHook);
                this.keyboardHook = IntPtr.Zero;
            }
        }
    }
}
