﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace ModernUI
{
    /// <summary>
    /// Interaction logic for MetroWindow.xaml
    /// </summary>
    public partial class ModernWindow : Window
    {
        public ModernWindow()
        {
            InitializeComponent();
            this.AeroSnapWindow = new AeroSnapWindow();
        }

        private AeroSnapWindow AeroSnapWindow;
        public IntPtr Handle { get; private set; }
        public WindowShadow Shadow { get; private set; }
        public KeyboardHook KeyboardHook { get; private set; }

        /// <summary>
        /// Gets the size and location of a window.
        /// </summary>
        private Rect Bounds
        {
            get { return new Rect { Width = this.Width, Height = this.Height, X = this.Left, Y = this.Top }; }
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            // enable drop shadow
            this.Shadow = new WindowShadow(this);
            this.Shadow.IsEnabled = true;
            this.Handle = this.Shadow.WindowHandle;
            // set low-level keyboard hook
            this.KeyboardHook = new KeyboardHook();
        }

        /// <summary>
        /// Set low-level keyboard hook to support windows native shortcuts (Win + SomeKey)
        /// </summary>
        private void SetKeyboardHooks()
        {
            this.KeyboardHook.SubscribeKeyEvent(new Action(this.Minimize),
                new Key[] { Key.LWin, Key.M },
                new Key[] { Key.RWin, Key.M }
            );
            this.KeyboardHook.SubscribeKeyEvent(new Action(this.Restore),
                new Key[] { Key.LWin, Key.LeftShift, Key.M },
                new Key[] { Key.LWin, Key.RightShift, Key.M },
                new Key[] { Key.RWin, Key.LeftShift, Key.M },
                new Key[] { Key.RWin, Key.RightShift, Key.M }
            );
            this.KeyboardHook.SubscribeKeyEvent(() =>
                {
                    if (!this.IsActive && this.WindowState != System.Windows.WindowState.Minimized)
                        this.Minimize();
                },
                new Key[] { Key.LWin, Key.Home },
                new Key[] { Key.RWin, Key.Home }
            );
        }

        private bool IsWinKeyPressed { get; set; }
        private bool IsShiftKeyPressed { get; set; }
        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.LWin || e.Key == Key.RWin) this.IsWinKeyPressed = true;
            else if (e.Key == Key.LeftShift || e.Key == Key.RightShift) this.IsShiftKeyPressed = true;

            base.OnPreviewKeyDown(e);
        }

        protected override void OnPreviewKeyUp(KeyEventArgs e)
        {
            if (this.IsWinKeyPressed)
            {
                switch (e.Key)
                {
                    case Key.Left:
                        if (this.IsShiftKeyPressed)
                        {
                            // TODO: Move window to monitor on the left
                        } 
                        else 
                            this.ChangeSnapState(WindowSnapState.Left);
                        break;
                    case Key.Right:
                        if (this.IsShiftKeyPressed)
                        {
                            // TODO: Move window to monitor on the right
                        }
                        else
                            this.ChangeSnapState(WindowSnapState.Right);
                        break;
                    case Key.Up:
                        if (this.IsShiftKeyPressed)
                            this.ChangeSnapState(WindowSnapState.FullHeight);
                        else
                            this.ChangeSnapState(WindowSnapState.Full);
                        break;
                    case Key.Down:
                        if (this.SnapState != WindowSnapState.None)
                            this.Restore();
                        else
                            this.Minimize();
                        break;
                    case Key.LWin:
                    case Key.RWin:
                        this.IsWinKeyPressed = false;
                        break;
                }
            }

            if (e.Key == Key.LeftShift || e.Key == Key.RightShift) this.IsShiftKeyPressed = false;
            base.OnPreviewKeyUp(e);
        }

        /// <summary>
        /// Gets the snap state of the window. This is a dependency property.
        /// </summary>
        public WindowSnapState SnapState
        {
            get { return (WindowSnapState)GetValue(SnapStateProperty); }
            private set { SetValue(SnapStateProperty, value); }
        }

        public static readonly DependencyProperty SnapStateProperty =
            DependencyProperty.Register("SnapState", typeof(WindowSnapState), 
                            typeof(Window), new PropertyMetadata(WindowSnapState.None));

        /// <summary>
        /// Sets window size and location to given boundaries.
        /// </summary>
        /// <param name="bounds">Struct holding size and location information.</param>
        private void SetWindowBounds(Rect bounds)
        {
            this.Width = bounds.Width;
            this.Height = bounds.Height;
            this.Top = bounds.Y;
            this.Left = bounds.X;

            if (this.WindowState == System.Windows.WindowState.Maximized) this.RestoreWindowState();
        }

        /// <summary>
        /// Gets the size and location of a window before being snapped.
        /// </summary>
        public Rect SnapRestoreBounds { get; private set; }

        /// <summary>
        /// Provides Aero Snap feature to a window that has WindowStyle set to None and Resize set to NoResize.
        /// </summary>
        /// <param name="snapState">State into which window should be placed</param>
        private void ChangeSnapState(WindowSnapState snapState)
        {
            // If already in place, then ignore
            if (this.SnapState == snapState) return;

            if ((this.SnapState == WindowSnapState.Left && snapState == WindowSnapState.Right) ||
                (this.SnapState == WindowSnapState.Right && snapState == WindowSnapState.Left) ||
                snapState == WindowSnapState.None) 
            {
                // Restore to normal
                this.SetWindowBounds(this.SnapRestoreBounds);
                this.SnapState = WindowSnapState.None;
            }
            else
            {
                // Remeber current size & location if window is in normal state
                if (this.SnapState == WindowSnapState.None && !IsDragged)
                    this.SnapRestoreBounds = this.RestoreBounds;

                this.SnapState = snapState;

                if (snapState == WindowSnapState.FullHeight)
                {
                    this.SetWindowBounds(new Rect
                    {
                        Width = this.RestoreBounds.Width,
                        Height = SystemParameters.WorkArea.Height,
                        X = this.RestoreBounds.X,
                        Y = 0
                    });
                }
                else if (snapState == WindowSnapState.Full)
                {
                    this.SetWindowBounds(new Rect
                    {
                        Width = SystemParameters.WorkArea.Width,
                        Height = SystemParameters.WorkArea.Height,
                        X = 0,
                        Y = 0
                    });
                    this.Maximize();
                }
                else if (snapState == WindowSnapState.Left)
                {
                    this.SetWindowBounds(new Rect
                    {
                        Width = SystemParameters.WorkArea.Width / 2,
                        Height = SystemParameters.WorkArea.Height,
                        X = 0,
                        Y = 0
                    });
                }
                else if (snapState == WindowSnapState.Right)
                {
                    this.SetWindowBounds(new Rect
                    {
                        Width = SystemParameters.WorkArea.Width / 2,
                        Height = SystemParameters.WorkArea.Height,
                        X = SystemParameters.WorkArea.Width/2,
                        Y = 0
                    });
                }
            }
        }

        /// <summary>
        /// Restores window's previous size and location if window is maximized or in aero snap state.
        /// </summary>
        public void Restore()
        {
            if (this.SnapState.IsOne(WindowSnapState.Full, WindowSnapState.Left, WindowSnapState.Right, WindowSnapState.FullHeight))
            {
                this.ChangeSnapState(WindowSnapState.None);
            }
            this.RestoreWindowState();
        }

        /// <summary>
        /// Set WindowState to Normal and change Restore button to Maximize button.
        /// </summary>
        private void RestoreWindowState()
        {
            this.WindowState = System.Windows.WindowState.Normal;
            MaximizeButton.Visibility = System.Windows.Visibility.Visible;
            RestoreButton.Visibility = System.Windows.Visibility.Hidden;
        }

        public void Maximize()
        {
            if (this.SnapState == WindowSnapState.None)
                this.SnapRestoreBounds = this.RestoreBounds;

            this.WindowState = System.Windows.WindowState.Maximized;
            MaximizeButton.Visibility = System.Windows.Visibility.Hidden;
            RestoreButton.Visibility = System.Windows.Visibility.Visible;
        }

        public void Minimize()
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        /// <summary>
        /// Adds a specified UIElement as a window content
        /// </summary>
        public new UIElement Content
        {
            get { return this.WindowContent.Child; }
            set { this.WindowContent.Child = value; }
        }

        /// <summary>
        /// Gets or sets the size of Minimize, Maximize, Restore & Close buttons on user interface (UI). 
        /// This is a dependency property.
        /// </summary>
        public TitleBarButtonSize TitleBarButtonSize
        {
            get { return (TitleBarButtonSize)GetValue(TitleBarButtonSizeProperty); }
            set { SetValue(TitleBarButtonSizeProperty, value); }
        }

        public static readonly DependencyProperty TitleBarButtonSizeProperty =
            DependencyProperty.Register("TitleBarButtonSize", typeof(TitleBarButtonSize),
                            typeof(Window), new PropertyMetadata(TitleBarButtonSize.Normal));

        /// <summary>
        /// Gets or sets the user interface (UI) visibility of this element. This is a dependency property.
        /// </summary>
        public Visibility IconVisibility
        {
            get { return (Visibility)GetValue(IconVisibilityProperty); }
            set { SetValue(IconVisibilityProperty, value); }
        }

        public static readonly DependencyProperty IconVisibilityProperty =
            DependencyProperty.Register("IconVisibility", typeof(Visibility), 
                            typeof(Window), new PropertyMetadata(Visibility.Visible));

        /// <summary>
        /// Gets or sets the user interface (UI) visibility of this element. 
        /// This is a dependency property.
        /// </summary>
        public Visibility TitleVisibility
        {
            get { return (Visibility)GetValue(TitleVisibilityProperty); }
            set { SetValue(TitleVisibilityProperty, value); }
        }

        public static readonly DependencyProperty TitleVisibilityProperty =
            DependencyProperty.Register("TitleVisibility", typeof(Visibility), 
                            typeof(Window), new PropertyMetadata(Visibility.Visible));

        //
        // Commands 
        //

        /// <summary>
        /// Causes the window to close when invoked.
        /// </summary>
        public Command CloseWindowCommand
        {
            get { return new Command(() => { this.Close(); }); }
        }

        /// <summary>
        /// Causes the window to maximize when invoked.
        /// </summary>
        public Command MaximizeWindowCommand
        {
            get { return new Command(() => { this.Maximize(); }); }
        }
        
        /// <summary>
        /// Causes the window to minimize when invoked.
        /// </summary>
        public Command MinimizeWindowCommand
        {
            get { return new Command(() => { this.Minimize(); }); }
        }

        /// <summary>
        /// Causes the window to restore to it's previous size and location.
        /// </summary>
        public Command RestoreWindowCommand
        {
            get { return new Command(() => { this.Restore(); }); }
        }

        //
        // Event Handlers
        //

        private void TitleBar_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
                this.Restore();
            else
                this.Maximize();
        }

        private void ResizeGrip_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.WindowState != System.Windows.WindowState.Maximized)
            {
                Rectangle rect = sender as Rectangle;
                Resize.ResizeWindow(this.Handle, (Resize.ResizeDirection)Enum.Parse(typeof(Resize.ResizeDirection), rect.Name.Replace("Grip", "")));
            }
        }

        private Point mouseDownPoint;

        private void TitleBar_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.mouseDownPoint = e.GetPosition(this);
        }

        public bool IsDragged
        {
            get { return (bool)GetValue(IsDraggedProperty); }
            set { SetValue(IsDraggedProperty, value); }
        }

        public static readonly DependencyProperty IsDraggedProperty =
            DependencyProperty.Register("IsDragged", typeof(bool), typeof(Window), new PropertyMetadata(false));

        private void TitleBar_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            Point cursorPos = e.GetPosition(this);
            if (e.LeftButton == MouseButtonState.Pressed && 
                (Math.Abs(this.mouseDownPoint.X - cursorPos.X) >= SystemParameters.MinimumHorizontalDragDistance / 2 ||
                Math.Abs(this.mouseDownPoint.Y - cursorPos.Y) >= SystemParameters.MinimumVerticalDragDistance / 2))
            {
                this.IsDragged = true;

                if (this.WindowState == System.Windows.WindowState.Maximized || this.SnapState.IsOne(WindowSnapState.Left, WindowSnapState.Right, WindowSnapState.FullHeight))
                {
                    Point offset = new Point
                    {
                        X = cursorPos.X / this.Width * this.SnapRestoreBounds.Width,
                        Y = cursorPos.Y / this.Height * this.SnapRestoreBounds.Height
                    };
                    this.SnapRestoreBounds = new Rect
                    {
                        Width = this.SnapRestoreBounds.Width,
                        Height = this.SnapRestoreBounds.Height,
                        X = cursorPos.X - offset.X + (this.SnapState == WindowSnapState.Right ? SystemParameters.WorkArea.Width / 2 : 0),
                        Y = cursorPos.Y - offset.Y
                    };
                    this.Restore();
                }
                else
                {
                    this.SnapRestoreBounds = new Rect
                    {
                        Width = this.Width,
                        Height = this.Height,
                        X = this.Left,
                        Y = this.Top
                    };
                }
                this.DragMove();
            }
        }

        protected override void OnLocationChanged(EventArgs e)
        {
            base.OnLocationChanged(e);
            if (this.IsDragged)
            {
                WindowSnapState snapState = Resize.GetCursorSnapState();
                if (snapState == WindowSnapState.None)
                    this.AeroSnapWindow.Hide();
                else
                    this.AeroSnapWindow.Show(snapState, this.Bounds);

                this.Focus();
            }
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            if (this.AeroSnapWindow.IsVisible)
            {
                this.ChangeSnapState(this.AeroSnapWindow.State);
                this.AeroSnapWindow.Hide();
            }
            if (this.IsDragged) this.IsDragged = false;

            base.OnMouseUp(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (this.KeyboardHook != null) this.KeyboardHook.Dispose();
            if (this.AeroSnapWindow != null) this.AeroSnapWindow.Close();
        }
    }
}
