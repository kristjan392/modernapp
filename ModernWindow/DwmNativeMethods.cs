﻿using System;
using System.Runtime.InteropServices;

namespace ModernUI
{
    internal static class DwmNativeMethods
    {
        [DllImport("dwmapi.dll", PreserveSig = true)]
        internal static extern int DwmSetWindowAttribute(IntPtr hwnd, DwmWindowAttribute attr, ref int attrValue, int attrSize);

        [DllImport("dwmapi.dll")]
        internal static extern int DwmExtendFrameIntoClientArea(IntPtr hWnd, ref WindowShadow.Margins pMarInset);
    }
}
