﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModernUI
{
    public enum WindowSnapState
    {
        None,
        Left,
        Right,
        FullHeight,
        Full
    }

    public static class WindowSnapStateExtension
    {
        public static bool IsOne(this WindowSnapState value, params WindowSnapState[] snapStates)
        {
            foreach (WindowSnapState state in snapStates)
            {
                if (value == state) return true;
            }
            return false;
        }
    }
}
