﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace ModernUI
{
    public class AnimatedElement : FrameworkElement
    {
        public double StartValue
        {
            get { return (double)GetValue(StartValueProperty); }
            set { SetValue(StartValueProperty, value); }
        }
        public static readonly DependencyProperty StartValueProperty =
            DependencyProperty.Register("StartValue", typeof(double), typeof(FrameworkElement), new PropertyMetadata(0.0));

        public double CurrentValue
        {
            get { return (double)GetValue(CurrentValueProperty); }
            set { SetValue(CurrentValueProperty, value); }
        }

        public static readonly DependencyProperty CurrentValueProperty =
            DependencyProperty.Register("CurrentValue", typeof(double), typeof(FrameworkElement), new PropertyMetadata(0.0));

        public double EndValue
        {
            get { return (double)GetValue(EndValueProperty); }
            set { SetValue(EndValueProperty, value); }
        }

        public static readonly DependencyProperty EndValueProperty =
            DependencyProperty.Register("EndValue", typeof(double), typeof(FrameworkElement), new PropertyMetadata(0.0));

        public double Progress
        {
            get { return (double)GetValue(ProgressProperty); }
            set { SetValue(ProgressProperty, value); }
        }

        public static readonly DependencyProperty ProgressProperty =
            DependencyProperty.Register("Progress", typeof(double), typeof(FrameworkElement), new PropertyMetadata(0.0, new PropertyChangedCallback(OnProgressAdvanced)));

        private static void OnProgressAdvanced(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ModernUI.AnimatedElement obj = d as ModernUI.AnimatedElement;
            if (obj != null)
            {
                obj.CurrentValue = (int)(obj.StartValue + (obj.EndValue - obj.StartValue) * obj.Progress);
            }
        }
    }
}
