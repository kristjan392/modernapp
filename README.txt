To make use of ModernWindow, just remove StartUpUri property from your application's App.xaml and
add the following to your application's App.xaml.cs:

protected override void OnStartup(StartupEventArgs e)
{
    base.OnStartup(e);

    ModernUI.ModernWindow window = new ModernUI.ModernWindow();
    window.Content = new WindowContent();
    window.Show();
}
