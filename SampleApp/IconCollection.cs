﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace SampleApp
{
    public class IconCollection
    {
        public string Source { get; private set; }
        public ReadOnlyCollection<BitmapFrame> Icons { get; private set; }

        public IconCollection(string uriSource) 
        {
            this.Source = uriSource;

            BitmapDecoder bitmapDecoder = IconBitmapDecoder.Create(
                new Uri("pack://application:,,,/" + uriSource),
                BitmapCreateOptions.None, BitmapCacheOption.OnDemand
            );

            this.Icons = bitmapDecoder.Frames;
        }

        public BitmapFrame GetIcon(int width)
        {
            var icon = this.Icons.FirstOrDefault(x => x.Width == width);
            if (icon == default(BitmapFrame))
            {
                icon = this.Icons.OrderBy(x => x.Width).First();
            }
            return icon;
        }
    }

    //public class IconConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
            
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}
