﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModernUI;

namespace SampleApp
{
    /// <summary>
    /// Interaction logic for WindowContent.xaml
    /// </summary>
    public partial class WindowContent : UserControl
    {
        public ModernWindow MainWindow { get; private set; }

        public WindowContent()
        {
            InitializeComponent();
            this.MainWindow = Application.Current.MainWindow as ModernWindow;
            this.MainWindow.Title = "ModernWindow Project";

            IconCollection icons = new IconCollection("Images/Windows_Icon.ico");
            this.MainWindow.Icon = icons.GetIcon(16);
            this.MainWindow.Width = 700;
            this.MainWindow.Height = 500;
        }

        private void ToggleButtonSizeButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.MainWindow != null)
            {
                if (this.MainWindow.TitleBarButtonSize == TitleBarButtonSize.Normal)
                {
                    this.MainWindow.TitleBarButtonSize = TitleBarButtonSize.Large;
                }
                else
                {
                    this.MainWindow.TitleBarButtonSize = TitleBarButtonSize.Normal;
                }
            }
        }

        private void ToggleTitleButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.MainWindow != null)
            {
                if (this.MainWindow.TitleVisibility == System.Windows.Visibility.Hidden)
                {
                    this.MainWindow.TitleVisibility = Visibility.Visible;
                }
                else
                {
                    this.MainWindow.TitleVisibility = Visibility.Hidden;
                }
            }
        }

        private void ToggleIconButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.MainWindow != null)
            {
                if (this.MainWindow.IconVisibility == System.Windows.Visibility.Collapsed)
                {
                    this.MainWindow.IconVisibility = System.Windows.Visibility.Visible;
                }
                else if(this.MainWindow.IconVisibility == System.Windows.Visibility.Visible)
                {
                    this.MainWindow.IconVisibility = Visibility.Hidden;
                }
                else
                {
                    this.MainWindow.IconVisibility = System.Windows.Visibility.Collapsed;
                }
            }
        }


    }
}
